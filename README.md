# Docker Servod Tools

Remake of https://gitlab.collabora.com/chromium/servod-tools, compatible with the dockerized version of the `hdctools` (see: https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/refs/heads/main/servo/dockerfiles/).

These tools start a `servod` container automatically upon the connection of a Servo/Servo Micro board or SuzyQable and map the `servod` port to a predictable port in the host for communication over XML-RPC API. The tools also create symlinks to the device's UARTs: `/dev/google-servo/$devicename/cpu-uart`, `/dev/google-servo/$devicename/ec-uart` and `/dev/google-servo/$devicename/cr50-uart`. The `servod` container is automatically stopped and removed when the device is unplugged.

# Dependencies

The `run-docker-servod` script requires the following Python3 packages to be installed:
* `python3-pyudev` (>= 0.21.0)
* `python3-docker` (>= 3.4.1)
* `python3-systemd` (>= 234)

# Set up and configuration

## udev

Udev rules to match the Servo/Servo Micro/SuzyQable devices and start a `servod` container automatically using `systemd`. Goes in `/lib/udev/rules.d/`.

## systemd

Unit configuration file for systemd, goes in `/usr/lib/systemd/system/`.

## conf

* `google-servo.conf` - servod configuration file, takes the form:

```
localalias,serial,port,board type,board model
```

* `run-docker-servod.conf` - configuration file for the run-docker-servod script; defines the URL and tag for the `servod` docker image and the path to the servod configuration file (optional; default is `/etc/google-servo.conf`)

### localalias

Unique alias for the board. It is used to name the respective `servod` container and for the UART symlinks: `/dev/google-servo/$devicename/*`.

### serial

The board serial number.

It's possible to get this information from `dmesg` after plugging the Servo/SuzyQable with the device attached to it.
For example:

```
sudo dmesg | grep 18d1 -A4
[    7.945500] usb 3-2.2.2.5: New USB device found, idVendor=18d1, idProduct=5002
[    7.945505] usb 3-2.2.2.5: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[    7.945508] usb 3-2.2.2.5: Product: servo:810-10010-02
[    7.945511] usb 3-2.2.2.5: Manufacturer: Google Inc
[    7.945513] usb 3-2.2.2.5: SerialNumber: 905537-00233
```

### port

The port number to bind to for communicating with `servod` from the host. `servod` is always launched inside the container on port 9999, which is then mapped to the port specified here.

### board type

Internal value used by servo to work out how to communicate with the target device; see [hdctools/servo/data/](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/refs/heads/main/servo/data/) for the full list of supported overlays.

### board model

Some board variants require a specific `servod` overlay different from the base board one; look in [hdctools/servo/data/](https://chromium.googlesource.com/chromiumos/third_party/hdctools/+/refs/heads/main/servo/data/) for possible board variant-specific overlays.

## run-docker-servod

Wrapper that runs out of systemd; starts a `servod` docker container and creates the links for the relevant device's UARTs.

Goes in `/usr/bin/run-docker-servod`.


